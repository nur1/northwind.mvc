﻿using NorthwindMVC.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NorthwindMVC.WebMVC.Models
{
    public class OrderModel
    {
        [Required]
        public Orders Order { get; set; }
         
        public List<OrderDetails> OrderDetails { get; set; }
    }
}