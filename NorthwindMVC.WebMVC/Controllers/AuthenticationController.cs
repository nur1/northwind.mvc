﻿using CodeID.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace NorthwindMVC.WebMVC.Controllers
{
    public class AuthenticationController : Controller
    {
        // GET: Authentication
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string userid, string password)
        {
            if (ModelState.IsValid)
            {
                if(UserValidation(userid, password))
                {
                    FormsAuthentication.SetAuthCookie(userid, false);
                    return Json(ApiResponse.OK(new { loggedIn= true, message="Login Success" }));
                }
                else
                {
                    return Json(ApiResponse.NotAcceptable("Invalid username or password "));
                }
            }
            else
            {
                return Json(ApiResponse.BadRequest(userid));
            }
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return Redirect("/Authentication");
        }

        private bool UserValidation(string userid, string password)
        {
            if (string.IsNullOrEmpty(userid)) return false;
            return (userid.ToLower().Equals("admin") && password.Equals("P@ssw0rd"));
        }
    }
}