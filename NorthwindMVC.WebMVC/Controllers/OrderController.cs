﻿using CodeID.Helper;
using Newtonsoft.Json;
using NorthwindMVC.Entity;
using NorthwindMVC.Facade;
using NorthwindMVC.WebMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace NorthwindMVC.WebMVC.Controllers
{
    [Authorize]
    public class OrderController : Controller
    {
        // GET: Order
        public ActionResult Index()
        { 
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Insert(OrderModel OrderModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var facade = new OrderFacade())
                    {
                        var status =  await facade.Insert(OrderModel.Order, OrderModel.OrderDetails);
                        if (status)
                        {
                            return Json(ApiResponse.OK(status));
                        }
                        else
                        {
                            return Json(ApiResponse.NotAcceptable(OrderModel));
                        }

                    }
                }
                catch (Exception ex)
                {
                    return Json(ApiResponse.InternalServerError(ex.Message));
                }
            }
            else
            {
                return Json(ApiResponse.BadRequest(OrderModel));
            }
        }

        [HttpPost]
        public async Task<ActionResult> Update(OrderModel OrderModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var facade = new OrderFacade())
                    {
                        var status = await facade.Update(OrderModel.Order, OrderModel.OrderDetails);
                        if (status)
                        {
                            return Json(ApiResponse.OK(status));
                        }
                        else
                        {
                            return Json(ApiResponse.NotAcceptable(OrderModel));
                        }

                    }
                }
                catch (Exception ex)
                {
                    return Json(ApiResponse.InternalServerError(ex.Message));
                }
            }
            else
            {
                return Json(ApiResponse.BadRequest(OrderModel));
            }
        }

        [HttpPost]
        public async Task<ActionResult> Paging(PageRequest page)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var facade = new OrderFacade())
                    {
                        var pgResult = await facade.GetPage(page);
                        var result = JsonConvert.SerializeObject(ApiResponse.OK(pgResult));
                        return Content(result, "application/json");
                    }
                }
                catch (Exception ex)
                {
                    return Json(ApiResponse.InternalServerError(ex.Message));
                }
            }
            else
            {
                return Json(ApiResponse.BadRequest(page));
            }
        }

        public ActionResult GetProducts(int id)
        {
            if (ModelState.IsValid)
            {
                using (var facade = new OrderDetailFacade())
                {
                    var listItems = facade.GetItemsByOrderID(id);
                    if (listItems == null)
                    {
                        return Json(ApiResponse.NotFound("Data not found"), JsonRequestBehavior.AllowGet);
                    }
                    return Json(ApiResponse.OK(listItems), JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(ApiResponse.BadRequest(id), JsonRequestBehavior.AllowGet);
            }
        }
    }
}