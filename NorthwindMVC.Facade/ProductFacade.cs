﻿using NorthwindMVC.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NorthwindMVC.Facade
{
    public class ProductFacade : BaseFacade<Products>
    {
        public DataTable GetProductByCategoryAsDataTable()
        {
            try
            {
                var conn = Context.Database.Connection;
                conn.Open();
                var cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT * FROM [dbo].[Products by Category]";
                var reader = cmd.ExecuteReader();
                DataTable dt = new DataTable("DataSet1");
                dt.Load(reader);
                return dt;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
