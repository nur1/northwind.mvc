﻿using CodeID.Helper;
using NorthwindMVC.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;

namespace NorthwindMVC.Facade
{
    public class OrderFacade : BaseFacade<Orders>
    {
        public override async Task<PageResult> GetPage(PageRequest option)
        {
            try
            {
                var result = await Task.Run(() => {
                    var where = " 1=1 ";
                    var param = new object[option.Criteria.Count];
                    int i = 0;
                    option.Criteria.ForEach(c => {
                        where += string.Format("AND {0}.ToString().Contains(@{1}) ", c.criteria, i);
                        param[i] = c.value;
                        i++;
                    });
                    var sql = @"SELECT * FROM (
	                            SELECT [OrderID]
                                  ,a.[CustomerID]
	                              ,b.CompanyName [CustomerName]
                                  ,[EmployeeID] 
                                  ,[OrderDate]
                                  ,[RequiredDate]
                                  ,[ShippedDate]
                                  ,[ShipVia]
                                  ,c.CompanyName [ShipperName]
                                  ,[Freight]
                                  ,[ShipName]
                                  ,[ShipAddress]
                                  ,[ShipCity]
                                  ,[ShipRegion]
                                  ,[ShipPostalCode]
                                  ,[ShipCountry]
                              FROM [dbo].[Orders] a
                              JOIN Customers b ON b.CustomerID = a.CustomerID 
                              JOIN Shippers c ON c.ShipperID = a.ShipVia 
                            )x";
                    var query = Context.Database.SqlQuery<OrdersView>(sql).Where(where, param);
                    var count = query.Count();
                    var rows = query.OrderBy(option.Order)
                        .Skip((option.Page - 1) * option.PageSize)
                        .Take(option.PageSize)
                        .ToList();
                    var pageCount = (int)Math.Ceiling(count * 1.0 / option.PageSize);
                    var pageResult = new PageResult { Rows = rows, RowCount = count, PageCount = pageCount };
                    return pageResult;
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> Insert(Orders order, List<OrderDetails> products)
        {
            try
            {
                order = Context.Orders.Add(order); 
                int affctd = await Context.SaveChangesAsync();
                products.ForEach(o => o.OrderID = order.OrderID);
                Context.OrderDetails.AddRange(products);
                affctd = await Context.SaveChangesAsync();
                return affctd > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> Update(Orders order, List<OrderDetails> products)
        {
            try
            {
                Context.Entry<Orders>(order).State = EntityState.Modified;

                Context.Database.ExecuteSqlCommand("DELETE FROM [Order Details] WHERE OrderID = {0}", new object[] { order.OrderID });
                products.ForEach(o => o.OrderID = order.OrderID);
                Context.OrderDetails.AddRange(products);
                int affctd = await Context.SaveChangesAsync();
                return affctd > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
