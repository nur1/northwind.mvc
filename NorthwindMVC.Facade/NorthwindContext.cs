﻿using NorthwindMVC.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NorthwindMVC.Facade
{
    public class NorthwindContext : DbContext
    {
        public NorthwindContext():base("NORTHWIND") { }

        public DbSet<Customers> Customers { get; set; }

        public DbSet<Shippers> Shippers { get; set; }

        public DbSet<Region> Region { get; set; }
        public DbSet<Products> Products { get; set; }
        public DbSet<Orders> Orders { get; set; }
        public DbSet<OrderDetails> OrderDetails { get; set; } 

        //
    }
}
