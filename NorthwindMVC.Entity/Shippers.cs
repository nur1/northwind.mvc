﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NorthwindMVC.Entity
{
    [Table("Shippers")]
    public class Shippers
    {
        [Key]
        public int ShipperID { get; set; }

        [Required]
        public string CompanyName { get; set; }

        public string Phone { get; set; }
    }
}
