﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NorthwindMVC.Entity
{
    [Table("Order Details")]
    public class OrderDetails
    {
        [Key, Column("OrderID", Order = 0)]
        public int OrderID { get; set; }

        [Key, Column("ProductID", Order = 1)]
        public int ProductID { get; set; }
        
        public decimal UnitPrice { get; set; }

        public Int16 Quantity { get; set; }

        public Single Discount { get; set; }
    }

    public class OrderDetailsView
    { 
        public int OrderID { get; set; }
         
        public int ProductID { get; set; }

        public string ProductName { get; set; }

        public decimal UnitPrice { get; set; }

        public Int16 Quantity { get; set; }

        public Single Discount { get; set; }
    }
}
