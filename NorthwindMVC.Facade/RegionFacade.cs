﻿using NorthwindMVC.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;

namespace NorthwindMVC.Facade
{
    public class RegionFacade : BaseFacade<Region>
    {
        public override async Task<bool> Insert(Region entity)
        {
            try
            {
                var reg = Context.Region.LastOrDefault();
                int id = reg == null ? 1 : (reg.RegionID + 1);                 
                entity.RegionID = id;
                Context.Region.Add(entity);
                int affctd = await Context.SaveChangesAsync();
                return affctd > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
