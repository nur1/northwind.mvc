﻿using Microsoft.Reporting.WebForms;
using NorthwindMVC.Facade;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using ReportViewerForMvc;

namespace NorthwindMVC.WebMVC.Controllers
{
    public class ReportController : Controller
    {
        // GET: Report
        public ActionResult Index()
        {
            using(var facade = new ProductFacade())
            {
                DataTable dt = facade.GetProductByCategoryAsDataTable();
                ReportDataSource ds = new ReportDataSource("DataSet1", dt);
                ReportViewer report = new ReportViewer();
                report.ProcessingMode = ProcessingMode.Local;
                report.SizeToReportContent = true;
                report.Width = Unit.Percentage(900);
                report.Height = Unit.Percentage(900);
                report.ZoomMode = ZoomMode.PageWidth;
                report.LocalReport.ReportPath = Server.MapPath("~/Reports/ProductsByCategory.rdlc");
                report.LocalReport.DataSources.Clear();
                report.LocalReport.DataSources.Add(ds);
                ViewBag.ReportProduct = report;
            }
            return View();
        }
    }
}