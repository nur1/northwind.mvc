﻿using NorthwindMVC.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NorthwindMVC.Facade
{
    public class OrderDetailFacade : BaseFacade<OrderDetails>
    {
        public IList<OrderDetailsView> GetItemsByOrderID(int id)
        {
            try
            {
                var sql = @"SELECT * FROM (
                              SELECT [OrderID]
                                  ,a.[ProductID]
	                              ,ProductName
                                  ,a.[UnitPrice]
                                  ,[Quantity]
                                  ,[Discount]
                              FROM [dbo].[Order Details] a
                              JOIN Products b ON b.ProductID = a.ProductID
                            )x";
                return Context.Database.SqlQuery<OrderDetailsView>(sql).Where(od => od.OrderID == id).ToList();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
