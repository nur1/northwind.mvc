﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NorthwindMVC.Entity
{
    [Table("Orders")]
    public class Orders
    {
        [Key]
        public int OrderID { get; set; }

        public string CustomerID { get; set; }

        public int EmployeeID { get; set; }

        [Required, JsonConverter(typeof(DateTimeConverter))]
        public DateTime OrderDate { get; set; }

        [Required, JsonConverter(typeof(DateTimeConverter))]
        public DateTime RequiredDate { get; set; }

        [JsonConverter(typeof(DateTimeConverter))]
        public DateTime? ShippedDate { get; set; }

        public int ShipVia { get; set; }

        public decimal Freight { get; set; }

        public string ShipName { get; set; }

        public string ShipAddress { get; set; }

        public string ShipCity { get; set; }

        public string ShipRegion { get; set; }

        public string ShipPostalCode { get; set; }

        public string ShipCountry { get; set; }
    }

    public class OrdersView
    {
        public int OrderID { get; set; }

        public string CustomerID { get; set; }

        public string CustomerName { get; set; }

        public int EmployeeID { get; set; }

        [Required, JsonConverter(typeof(DateTimeConverter))]
        public DateTime OrderDate { get; set; }

        [Required, JsonConverter(typeof(DateTimeConverter))]
        public DateTime RequiredDate { get; set; }

        [JsonConverter(typeof(DateTimeConverter))]
        public DateTime? ShippedDate { get; set; }

        public int ShipVia { get; set; }
        public string ShipperName { get; set; }

        public decimal Freight { get; set; }

        public string ShipName { get; set; }

        public string ShipAddress { get; set; }

        public string ShipCity { get; set; }

        public string ShipRegion { get; set; }

        public string ShipPostalCode { get; set; }

        public string ShipCountry { get; set; }
    }

    public class DateTimeConverter : IsoDateTimeConverter
    {
        public DateTimeConverter() {
            DateTimeFormat = "yyyy-MMM-dd";
        }
    }
}
